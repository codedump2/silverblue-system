For basic installation, enter the default toolbox and type:

    $ sudo dnf install ansible
    $ ansible-playbook aristoteles.yml

This will install the host system with flatpaks and default
toolbox with general purpose applications.

For every other toolbox (esp32, stm32, ...) do:

    $ toolbox create <toolbox>
    $ toolbox enter <toolbox>
    $ sudo dnf install ansible
    $ sudo hostname <toolbox>
    $ sudo ansible-playbook <toolbox>.yml

Enjoy.
